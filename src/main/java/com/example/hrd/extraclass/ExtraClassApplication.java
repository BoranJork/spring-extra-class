package com.example.hrd.extraclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExtraClassApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExtraClassApplication.class, args);
    }

}
