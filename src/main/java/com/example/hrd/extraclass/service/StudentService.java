package com.example.hrd.extraclass.service;


import com.example.hrd.extraclass.model.Students;

import java.util.List;
public interface StudentService {
    public List<Students> getAll();
}
